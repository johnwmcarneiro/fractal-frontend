import { combineReducers } from 'redux'
import { reducer as formReducer } from 'redux-form'
import { reducer as toastrReducer } from 'react-redux-toastr'

import authReducer from '../auth/authReducer'
import personReducer from '../person/personReducer'

const rootReducer = combineReducers({
  toastr: toastrReducer,
  form: formReducer,
  auth: authReducer,
  person: personReducer
})

export default rootReducer