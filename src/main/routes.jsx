import React from 'react'
import { Router, Route, IndexRoute, Redirect, hashHistory } from 'react-router'

import AuthOrApp from './authOrApp'
import Person from '../person/person'

export default props => (
  <Router history={hashHistory}>
    <Router path='/' component={AuthOrApp}>
      <IndexRoute component={Person} />
    </Router>
    <Redirect from='*' to='/' />
  </Router>
)