import React from 'react'
import axios from 'axios'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

import App from './app'
import Auth from '../auth/auth'

import '../common/template/dependencies'

class AuthOrApp extends React.Component {
  render() {
    const { user } = this.props.auth

    if (user) {
      axios.defaults.headers.common['X-User-Email'] = user.email
      axios.defaults.headers.common['X-User-Token'] = user.token
      
      return <App>{this.props.children}</App>
    } else {
      return <Auth />
    }
  }
}

const mapStateToProps = state => ({ auth: state.auth })
export default connect(mapStateToProps, null)(AuthOrApp)