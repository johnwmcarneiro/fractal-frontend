import React from 'react'

export default props => {
  const rows = props.rows
  const list = rows.map((row) => (
    <li key={row.id}>
      <span className="label label-info">{row.name}</span>
    </li>
  ))
  return (
    <div>
      <ul className="list-inline">
        {list}
      </ul>
    </div>
  )
}