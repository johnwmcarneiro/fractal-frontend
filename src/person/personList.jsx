import React from 'react'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import { getList, remove } from './personActions'

import SearchForm from './searchForm'
import ContactData from './contactData'
import Addresses from './addresses'
import Categories from './categories'

class PersonList extends React.Component {
  componentWillMount() {
    this.props.getList()
  }

  renderRows() {
    const list = this.props.list || []
    return list.map(p => (
      <div className="col-md-6" key={p.id}>
        <div className="media">
          <div className="media-body">
            <button className="btn btn-danger btn-sm pull-right" onClick={() => this.props.remove(p)}>
              <i className="fa fa-trash-o"></i>
            </button>
            <h4 className="media-object">{p.name}</h4>
            <p>{p.title}, {p.company}</p>
            <p><i className="fa fa-calendar"></i> {p.born_at}</p>

            <Categories rows={p.categories} />
            <ContactData rows={p.contact_data} />
            <Addresses rows={p.addresses} />
          </div>
        </div>
      </div>
    ))
  }

  renderPagination() {
    const meta = this.props.meta

    if (meta) {
      return (
        <nav>
          <ul className="pager">
            {meta.prev_page > 0 &&
              <li className="previous">
                <a
                  href="#"
                  onClick={() => this.props.getList(meta.prev_page)}
                >
                  Página anterior
                </a>
              </li>
            }

            {meta.next_page > 0 &&
              <li className="next">
                <a
                  href="#"
                  onClick={() => this.props.getList(meta.next_page)}
                >
                  Próxima página
                </a>
              </li>
            }
          </ul>
        </nav>
      )
    }
  }

  render() {
    return (
      <div>
        <div className="page-header">
          <h1>Lista de contatos <small>{this.props.meta.total_entries} registros</small></h1>
        </div>

        <SearchForm />

        <div className="row">
          {this.renderRows()}
        </div>

        {this.renderPagination()}
      </div>
    )
  }
}

const mapStateToProps = state => ({list: state.person.list, meta: state.person.meta})
const mapDispatchToProps = dispatch => bindActionCreators({getList, remove}, dispatch)
export default connect(mapStateToProps, mapDispatchToProps)(PersonList)