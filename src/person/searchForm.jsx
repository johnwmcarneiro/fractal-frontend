import React from 'react'
import { reduxForm, Field } from 'redux-form'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

import { getList } from './personActions'

class SearchForm extends React.Component {
  render() {
    const { handleSubmit, getList } = this.props

    return (
      <form
        role="form"
        className="form-inline form-search"
        onSubmit={handleSubmit(v => getList(1, v))}
      >
        <div className="input-group">
          <Field
            component="input"
            name="q"
            type="text"
            className="form-control"
          />
          <span className="input-group-btn">
            <button type="submit" className="btn btn-default">Buscar</button>
          </span>
        </div>
      </form>
    )
  }
}

SearchForm = reduxForm({ form: 'personSearchForm' })(SearchForm)
const mapDispatchToProps = dispatch => bindActionCreators({ getList }, dispatch)
export default connect(null, mapDispatchToProps)(SearchForm)
