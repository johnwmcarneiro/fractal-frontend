import React from 'react'

import Header from '../common/template/header'
import PersonList from './personList'

export default props => (
  <div>
    <Header />
    <PersonList />
  </div>
)