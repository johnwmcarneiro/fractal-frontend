import axios from 'axios'
import configs from '../configs/app'
import { toastr } from 'react-redux-toastr'

export function getList(page = 1, values = null) {
  const query = values && values.q ? `&q=${values.q}` : ''
  const request = axios['get'](`${configs.API_URL}/people?page=${page}${query}`, values)
  return {
    type: 'PEOPLE_FETCHED',
    payload: request
  }
}

export function remove(values) {
  return submit(values, 'delete')
}

function submit(values, method) {
  return dispatch => {
    const id = values.id ? values.id : ''
    axios[method](`${configs.API_URL}/people/${id}`, values)
      .then(resp => {
        toastr.success('Sucesso', 'Operação realizada com sucesso.')
        dispatch(getList())
      })
      .catch(e => {
        e.response.data.errors.forEach(error => toastr.error('Erro', error))
      })
  }
}