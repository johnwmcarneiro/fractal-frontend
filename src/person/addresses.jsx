import React from 'react'

export default props => {
  const rows = props.rows
  const list = rows.map((row) => (
    <li key={row.id}>
      {row.street}, {row.number}, {row.complement}, {row.neighborhood}, {row.city}, {row.state}, {row.zipcode}
    </li>
  ))
  return (
    <div>
      <strong>Endereço(s)</strong>
      <ul className="list-unstyled">{list}</ul>
    </div>
  )
}