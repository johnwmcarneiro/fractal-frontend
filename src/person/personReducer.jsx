const INITIAL_STATE = { list: [], meta: {} }

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case 'PEOPLE_FETCHED':
      return { ...state, list: action.payload.data.people, meta: action.payload.data.meta }
    default:
      return state
  }
}