import React from 'react'

export default props => {
  const rows = props.rows
  const list = rows.map((row) => (
    <li key={row.id}>
      {row.data_type}: {row.value}
    </li>
  ))
  return (
    <div>
      <strong>Informações de contato</strong>
      <ul className="list-unstyled">{list}</ul>
    </div>
  )
}