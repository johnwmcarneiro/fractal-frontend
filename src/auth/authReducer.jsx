const userKey = '_fractal_user'
const INITIAL_STATE = { user: JSON.parse(localStorage.getItem(userKey)) }

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case 'USER_FETCHED':
      localStorage.setItem(userKey, JSON.stringify(action.payload))
      return { ...state, user: action.payload }
      break;

    case 'USER_LOGOUT':
      return { ...state, user: null }
      break;
  
    default:
      return state
  }
}