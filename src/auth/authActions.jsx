import axios from 'axios'
import configs from '../configs/app'
import { toastr } from 'react-redux-toastr'

export function login(values) {
  return submit(values, `${configs.API_URL}/sessions`)
}

function submit(values, url) {
  return dispatch => {
    axios.post(url, values)
      .then(resp => {
        dispatch([
          { type: 'USER_FETCHED', payload: resp.data }
        ])
      })
      .catch(e => {
        toastr.error('Error', e.response.data.message)
      })
  }
}

export function logout() {
  return { type: 'USER_LOGOUT' }
}