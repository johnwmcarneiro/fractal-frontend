import React from 'react'
import { reduxForm, Field } from 'redux-form'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

import { login } from './authActions'
import Messages from '../common/msg/messages'

class Auth extends React.Component {
  onSubmit(values) {
    this.props.login(values)
  }

  render() {
    const { handleSubmit } = this.props

    return (
      <div className="row">
        <div className="col-md-4 col-md-offset-4">
          <h2>Login</h2>
          <form onSubmit={handleSubmit(v => this.onSubmit(v))}>
            <div className="form-group">
              <label>E-mail</label>
              <Field
                component="input"
                name="email"
                type="text"
                className="form-control"
              />
            </div>

            <div className="form-group">
              <label>Senha</label>
              <Field
                component="input"
                name="password"
                type="password"
                className="form-control"
              />
            </div>

            <button
              type="submit"
              className="btn btn-primary"
            >
              Entrar
            </button>
          </form>
          <Messages />
        </div>
      </div>
    )
  }
}

Auth = reduxForm({ form: 'authForm' })(Auth)
const mapDispatchToProps = dispatch => bindActionCreators({ login }, dispatch)
export default connect(null, mapDispatchToProps)(Auth)