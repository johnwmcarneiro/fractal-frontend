# README

Aplicação de "frontend" para consumir a API de contatos (https://gitlab.com/johnwmcarneiro/fractal).

### Funções
- Login na API
- Listagem dos contatos com paginação
- Busca textual dos contatos
- Exclusão de contatos
- Adicionar contato - PARA FAZER
- Editar contato - PARA FAZER

## Requisitos
- NPM
- NodeJS

## Instalação
Para instalar a aplicação, abra seu terminal e navegue até a pasta do projeto (```cd diretorio_do_projeto```), depois execute os comandos abaixo.
```
npm install
```

## Como usar
Para executar o projeto é necessário que tenha o projeto em backend iniciado (na porta 300) e em seu terminal rode o seguinte comando (deve estar na pasta do projeto).
```
npm run dev
```
Irá ser iniciado o servidor e responderá na porta 8080 (http://localhost:8080).

Depois basta informar os dados de acesso abaixo na tela de login.

e-mail: test@example.com
senha: 123456